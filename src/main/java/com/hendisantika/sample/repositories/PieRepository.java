package com.hendisantika.sample.repositories;

import com.hendisantika.sample.models.Pie;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 8/10/17
 * Time: 05.34
 * To change this template use File | Settings | File Templates.
 */
public interface PieRepository extends CrudRepository<Pie, Long> {
    List<Pie> findByName(String name);
}
